/*
 * Model loader and database for NetworkSpec's and FeatureSpec's.
 */

#include "MulticlassInference/MulticlassInference/interface/Loader.h"

namespace hmc {

bool Loader::hasModel(int year, const std::string& version, const std::string& tag) const {
  for (Model* model : models_) {
    if (model->getYear() == year && model->getVersion() == version && model->getTag() == tag) {
      return true;
    }
  }
  return false;
}

Model* Loader::loadModel(int year, const std::string& version, const std::string& tag) {
  const NetworkSpec& networkSpec = database_.getNetworkSpec(year, version, tag);

  if (hasModel(year, version, tag)) {
    throw std::runtime_error(
        "MulticlassInference Loader : model already loaded for " + networkSpec.getDescription());
  }

  models_.push_back(new Model(&networkSpec));
  return models_.back();
}

Model* Loader::getModel(int year, const std::string& version, const std::string& tag) {
  for (Model* model : models_) {
    if (model->getYear() == year && model->getVersion() == version && model->getTag() == tag) {
      return model;
    }
  }

  throw std::runtime_error("MulticlassInference Loader : model not found");
}

} // namespace hmc
