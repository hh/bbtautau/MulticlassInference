/*
 * Model definition.
 */

#include "MulticlassInference/MulticlassInference/interface/Model.h"

namespace hmc {

Model::Model(const NetworkSpec* networkSpec)
    : networkSpec_(networkSpec)
    , input(getFeatureNames())
    , output(getAllNodeNames()) {
  // load graphs
  graphDefEven_ = hmctf::loadGraphDef(networkSpec_->getGraphPathEven());
  graphDefOdd_ = hmctf::loadGraphDef(networkSpec_->getGraphPathOdd());

  // create sessions
  sessionEven_ = hmctf::createSession(graphDefEven_);
  sessionOdd_ = hmctf::createSession(graphDefOdd_);

  printInfo();
}

Model::~Model() {
  // close sessions
  if (sessionEven_ != nullptr) {
    hmctf::closeSession(sessionEven_);
    sessionEven_ = nullptr;
  }
  if (sessionOdd_ != nullptr) {
    hmctf::closeSession(sessionOdd_);
    sessionOdd_ = nullptr;
  }

  // delete graphs
  if (graphDefEven_ != nullptr) {
    delete graphDefEven_;
    graphDefEven_ = nullptr;
  }
  if (graphDefOdd_ != nullptr) {
    delete graphDefOdd_;
    graphDefOdd_ = nullptr;
  }
}

void Model::printInfo() const {
  std::cout << "--- Model information ------------------" << std::endl;
  std::cout << "Year         : " << getYear() << std::endl;
  std::cout << "Tag          : " << getTag() << std::endl;
  std::cout << "Version      : " << getVersion() << std::endl;
  std::cout << "Input tensor : " << getInputTensorName() << std::endl;
  std::cout << "Output tensor: " << getOutputTensorName() << std::endl;
  std::cout << "Features     : " << getNumberOfFeatures() << std::endl;
  std::cout << "Output nodes : " << getNumberOfNodes() << std::endl;
  for (const auto& nodeName : getNodeNames()) {
    std::cout << "    - " << nodeName << std::endl;
  }
  std::cout << "Merged nodes : " << getNodeMerging().size() << std::endl;
  for (const auto& pair : getNodeMerging()) {
    std::cout << "    - " << pair.first << ":";
    for (const int& i : pair.second) {
      std::cout << " " << getNodeName(i) << "(" << i << ")";
    }
    std::cout << std::endl;
  }
  std::cout << "----------------------------------------" << std::endl;
}

void Model::run(
    EventId eventId, const InputSpec& inputSpec, OutputSpec& outputSpec, bool checkDefaults) {
  // check if the input spec is complete, i.e., if all values were set
  if (!inputSpec.complete()) {
    throw std::runtime_error("MulticlassInference " + getDescription()
        + ": input spec is incomplete, only " + std::to_string(inputSpec.getNumberOfSetFeatures())
        + " out of " + std::to_string(inputSpec.getNumberOfFeatures()) + " features set");
  }

  // define the output and run
  std::vector<float> output;
  run(eventId, inputSpec.getValues(), output, checkDefaults);

  // extend outputs by merged nodes
  for (const auto& pair : getNodeMerging()) {
    float merged = 0.;
    for (const int& i : pair.second) {
      merged += output[i];
    }
    output.push_back(merged);
  }

  // set the output spec
  outputSpec.setOutput(output);
}

void Model::run(EventId eventId, const std::vector<float>& input, std::vector<float>& output,
    bool checkDefaults) {
  // check the input shape
  if (input.size() != getNumberOfFeatures()) {
    throw std::runtime_error("MulticlassInference " + getDescription() + ": got "
        + std::to_string(input.size()) + " input features, " + std::to_string(getNumberOfFeatures())
        + " are expected");
  }

  // clear outputs
  output.clear();

  // create and fill the input tensor
  tensorflow::Tensor inputTensor = createInputTensor();
  float* d = inputTensor.flat<float>().data();
  for (size_t i = 0; i < input.size(); i++, d++) {
    *d = input[i];
  }

  // run
  tensorflow::Tensor outputTensor = run(eventId, inputTensor, checkDefaults);

  // check the output shape
  int nNodes = getNumberOfNodes();
  if (outputTensor.shape().dims() != 2 || outputTensor.shape().dim_size(1) != nNodes) {
    throw std::runtime_error(
        "MulticlassInference " + getDescription() + ": output shape does not match expected shape");
  }

  // fill outputs
  d = outputTensor.flat<float>().data();
  output.resize(nNodes);
  for (int i = 0; i < nNodes; i++, d++) {
    output[i] = *d;
  }
}

tensorflow::Tensor Model::run(EventId eventId, tensorflow::Tensor& input, bool checkDefaults) {
  // the model contained in the even (odd) session was trained using events with even (odd)
  // event numbers, so do the oppositve here and evaluate with odd (even) events
  tensorflow::Session* session = (eventId % 2 == 0) ? sessionOdd_ : sessionEven_;

  // check the input shape
  int nFeatures = getNumberOfFeatures();
  if (input.shape().dims() != 2 || input.shape().dim_size(1) != nFeatures) {
    throw std::runtime_error(
        "MulticlassInference " + getDescription() + ": input shape does not match expected shape");
  }

  // check defaults
  if (checkDefaults) {
    const std::vector<FeatureSpec>& featureSpecs = getFeatureSpecs();
    float* d = input.flat<float>().data();
    int batchSize = input.shape().dim_size(0);
    for (int b = 0; b < batchSize; b++) {
      for (int f = 0; f < nFeatures; f++, d++) {
        if (featureSpecs[f].isMissingValue(*d)) {
          *d = featureSpecs[f].getDefaultValue();
        }
      }
    }
  }

  // define the output and run
  std::vector<tensorflow::Tensor> outputs;
  hmctf::run(session, { { getInputTensorName(), input } }, { getOutputTensorName() }, &outputs);

  return outputs[0];
}

} // namespace hmc
