/*
 * TensorFlow helpers, extracted from PhysicsTools/TensorFlow.
 */

#include "MulticlassInference/MulticlassInference/interface/TensorFlow.h"

namespace hmctf {

void setLogging(const std::string& level) {
  setenv("TF_CPP_MIN_LOG_LEVEL", level.c_str(), 0);
}

tensorflow::GraphDef* loadGraphDef(const std::string& pbFile) {
  // objects to load the graph
  tensorflow::Status status;

  // load it
  tensorflow::GraphDef* graphDef = new tensorflow::GraphDef();
  status = tensorflow::ReadBinaryProto(tensorflow::Env::Default(), pbFile, graphDef);

  // check for success
  if (!status.ok()) {
    throw std::runtime_error(
        "MulticlassInference InvalidGraphDef: error while loading graphDef from '" + pbFile
        + "': " + status.ToString());
  }

  return graphDef;
}

tensorflow::Session* createSession(tensorflow::GraphDef* graphDef, int nThreads) {
  // check for valid pointer
  if (graphDef == nullptr) {
    throw std::runtime_error(
        "MultclassInference InvalidGraphDef: error while creating session: graphDef is nullptr");
  }

  // check that the graph has nodes
  if (graphDef->node_size() <= 0) {
    throw std::runtime_error(
        "MultclassInference InvalidGraphDef: error while creating session: graphDef has no nodes");
  }

  // create session options and set thread options
  tensorflow::SessionOptions sessionOptions;
  sessionOptions.config.set_intra_op_parallelism_threads(nThreads);
  sessionOptions.config.set_inter_op_parallelism_threads(nThreads);

  // create a new, empty session
  tensorflow::Status status;
  tensorflow::Session* session = nullptr;
  status = tensorflow::NewSession(sessionOptions, &session);
  if (!status.ok()) {
    throw std::runtime_error("MultclassInference InvalidSession: error while creating session: " + status.ToString());
  }

  // add the graph def
  status = session->Create(*graphDef);
  if (!status.ok()) {
    throw std::runtime_error(
        "MultclassInference InvalidSession: error while attaching graphDef to session: "
        + status.ToString());
  }

  return session;
}

bool closeSession(tensorflow::Session*& session) {
  if (session == nullptr) {
    return true;
  }

  // close and delete the session
  tensorflow::Status status = session->Close();
  delete session;

  // reset the pointer
  session = nullptr;

  return status.ok();
}

void run(tensorflow::Session* session, const NamedTensorList& inputs,
    const std::vector<std::string>& outputNames, std::vector<tensorflow::Tensor>* outputs) {
  if (session == nullptr) {
    throw std::runtime_error("MultclassInference InvalidSession: cannot run empty session");
  }

  tensorflow::thread::ThreadPoolOptions threadPoolOptions;
  threadPoolOptions.inter_op_threadpool = &NoThreadPool::instance();
  threadPoolOptions.intra_op_threadpool = &NoThreadPool::instance();

  // create empty run options
  tensorflow::RunOptions runOptions;

  // run and check the status
  tensorflow::Status status
      = session->Run(runOptions, inputs, outputNames, {}, outputs, nullptr, threadPoolOptions);
  if (!status.ok()) {
    throw std::runtime_error(
        "MultclassInference InvalidRun: error while running session: " + status.ToString());
  }
}

} // namespace hmctf
