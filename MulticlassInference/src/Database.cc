/*
 * Database for NetworkSpec's and FeatureSpec's.
 */

#include "MulticlassInference/MulticlassInference/interface/Database.h"

namespace hmc {

void Database::setupFeatureSpecs() {
  addFeatureSpec(FeatureSpec("is_mutau", -1.0));
  addFeatureSpec(FeatureSpec("is_etau", -1.0));
  addFeatureSpec(FeatureSpec("is_tautau", -1.0));
  addFeatureSpec(FeatureSpec("is_2016", -1.0));
  addFeatureSpec(FeatureSpec("is_2017", -1.0));
  addFeatureSpec(FeatureSpec("is_2018", -1.0));
  addFeatureSpec(FeatureSpec("n_jets_20", -1.0));
  addFeatureSpec(FeatureSpec("n_bjets_20", -1.0));
  addFeatureSpec(FeatureSpec("is_vbf_loose_cat", -1.0));
  addFeatureSpec(FeatureSpec("is_vbf_tight_cat", -1.0));
  addFeatureSpec(FeatureSpec("is_resolved_1b_cat", -1.0));
  addFeatureSpec(FeatureSpec("is_resolved_2b_cat", -1.0));
  addFeatureSpec(FeatureSpec("is_boosted_cat", -1.0));
  addFeatureSpec(FeatureSpec("bjet1_pt", -1.0));
  addFeatureSpec(FeatureSpec("bjet1_eta", -5.0));
  addFeatureSpec(FeatureSpec("bjet1_phi", -4.0, { -1.0 }));
  addFeatureSpec(FeatureSpec("bjet1_e", -1.0));
  addFeatureSpec(FeatureSpec("bjet1_deepflavor_b", -1.0));
  addFeatureSpec(FeatureSpec("bjet1_deepflavor_cvsb", -1.0));
  addFeatureSpec(FeatureSpec("bjet1_deepflavor_cvsl", -1.0));
  addFeatureSpec(FeatureSpec("bjet1_hhbtag", -1.0));
  addFeatureSpec(FeatureSpec("bjet2_pt", -1.0));
  addFeatureSpec(FeatureSpec("bjet2_eta", -5.0));
  addFeatureSpec(FeatureSpec("bjet2_phi", -4.0, { -1.0 }));
  addFeatureSpec(FeatureSpec("bjet2_e", -1.0));
  addFeatureSpec(FeatureSpec("bjet2_deepflavor_b", -1.0));
  addFeatureSpec(FeatureSpec("bjet2_deepflavor_cvsb", -1.0));
  addFeatureSpec(FeatureSpec("bjet2_deepflavor_cvsl", -1.0));
  addFeatureSpec(FeatureSpec("bjet2_hhbtag", -1.0));
  addFeatureSpec(FeatureSpec("ctjet1_pt", -1.0));
  addFeatureSpec(FeatureSpec("ctjet1_eta", -5.0));
  addFeatureSpec(FeatureSpec("ctjet1_phi", -4.0, { -1.0 }));
  addFeatureSpec(FeatureSpec("ctjet1_e", -1.0));
  addFeatureSpec(FeatureSpec("ctjet1_deepflavor_b", -1.0));
  addFeatureSpec(FeatureSpec("ctjet1_hhbtag", -1.0));
  addFeatureSpec(FeatureSpec("ctjet2_pt", -1.0));
  addFeatureSpec(FeatureSpec("ctjet2_eta", -5.0));
  addFeatureSpec(FeatureSpec("ctjet2_phi", -4.0, { -1.0 }));
  addFeatureSpec(FeatureSpec("ctjet2_e", -1.0));
  addFeatureSpec(FeatureSpec("ctjet2_deepflavor_b", -1.0));
  addFeatureSpec(FeatureSpec("ctjet2_hhbtag", -1.0));
  addFeatureSpec(FeatureSpec("ctjet3_pt", -1.0));
  addFeatureSpec(FeatureSpec("ctjet3_eta", -5.0));
  addFeatureSpec(FeatureSpec("ctjet3_phi", -4.0, { -1.0 }));
  addFeatureSpec(FeatureSpec("ctjet3_e", -1.0));
  addFeatureSpec(FeatureSpec("ctjet3_deepflavor_b", -1.0));
  addFeatureSpec(FeatureSpec("ctjet3_hhbtag", -1.0));
  addFeatureSpec(FeatureSpec("fwjet1_pt", -1.0));
  addFeatureSpec(FeatureSpec("fwjet1_eta", -5.0));
  addFeatureSpec(FeatureSpec("fwjet1_phi", -4.0, { -1.0 }));
  addFeatureSpec(FeatureSpec("fwjet1_e", -1.0));
  addFeatureSpec(FeatureSpec("fwjet2_pt", -1.0));
  addFeatureSpec(FeatureSpec("fwjet2_eta", -5.0));
  addFeatureSpec(FeatureSpec("fwjet2_phi", -4.0, { -1.0 }));
  addFeatureSpec(FeatureSpec("fwjet2_e", -1.0));
  addFeatureSpec(FeatureSpec("jet3_pt", -1.0));
  addFeatureSpec(FeatureSpec("jet3_eta", -5.0));
  addFeatureSpec(FeatureSpec("jet3_phi", -4.0));
  addFeatureSpec(FeatureSpec("jet3_e", -1.0));
  addFeatureSpec(FeatureSpec("jet3_deepflavor_b", -1.0));
  addFeatureSpec(FeatureSpec("jet4_pt", -1.0));
  addFeatureSpec(FeatureSpec("jet4_eta", -5.0));
  addFeatureSpec(FeatureSpec("jet4_phi", -4.0));
  addFeatureSpec(FeatureSpec("jet4_e", -1.0));
  addFeatureSpec(FeatureSpec("jet4_deepflavor_b", -1.0));
  addFeatureSpec(FeatureSpec("jet5_pt", -1.0));
  addFeatureSpec(FeatureSpec("jet5_eta", -5.0));
  addFeatureSpec(FeatureSpec("jet5_phi", -4.0));
  addFeatureSpec(FeatureSpec("jet5_e", -1.0));
  addFeatureSpec(FeatureSpec("jet5_deepflavor_b", -1.0));
  addFeatureSpec(FeatureSpec("vbfjet1_pt", -1.0));
  addFeatureSpec(FeatureSpec("vbfjet1_eta", -5.0));
  addFeatureSpec(FeatureSpec("vbfjet1_phi", -4.0));
  addFeatureSpec(FeatureSpec("vbfjet1_e", -1.0));
  addFeatureSpec(FeatureSpec("vbfjet1_deepflavor_b", -1.0));
  addFeatureSpec(FeatureSpec("vbfjet1_deepflavor_cvsb", -1.0));
  addFeatureSpec(FeatureSpec("vbfjet1_deepflavor_cvsl", -1.0));
  addFeatureSpec(FeatureSpec("vbfjet1_hhbtag", -1.0));
  addFeatureSpec(FeatureSpec("vbfjet2_pt", -1.0));
  addFeatureSpec(FeatureSpec("vbfjet2_eta", -5.0));
  addFeatureSpec(FeatureSpec("vbfjet2_phi", -4.0));
  addFeatureSpec(FeatureSpec("vbfjet2_e", -1.0));
  addFeatureSpec(FeatureSpec("vbfjet2_deepflavor_b", -1.0));
  addFeatureSpec(FeatureSpec("vbfjet2_deepflavor_cvsb", -1.0));
  addFeatureSpec(FeatureSpec("vbfjet2_deepflavor_cvsl", -1.0));
  addFeatureSpec(FeatureSpec("vbfjet2_hhbtag", -1.0));
  addFeatureSpec(FeatureSpec("lep1_pt", -1.0));
  addFeatureSpec(FeatureSpec("lep1_eta", -5.0));
  addFeatureSpec(FeatureSpec("lep1_phi", -4.0));
  addFeatureSpec(FeatureSpec("lep1_e", -1.0));
  addFeatureSpec(FeatureSpec("lep2_pt", -1.0));
  addFeatureSpec(FeatureSpec("lep2_eta", -5.0));
  addFeatureSpec(FeatureSpec("lep2_phi", -4.0));
  addFeatureSpec(FeatureSpec("lep2_e", -1.0));
  addFeatureSpec(FeatureSpec("met_pt", -1.0));
  addFeatureSpec(FeatureSpec("met_phi", -4.0));
  addFeatureSpec(FeatureSpec("bh_pt", -1.0));
  addFeatureSpec(FeatureSpec("bh_eta", -5.0));
  addFeatureSpec(FeatureSpec("bh_phi", -4.0));
  addFeatureSpec(FeatureSpec("bh_e", -1.0));
  addFeatureSpec(FeatureSpec("tauh_sv_pt", -1.0));
  addFeatureSpec(FeatureSpec("tauh_sv_eta", -5.0));
  addFeatureSpec(FeatureSpec("tauh_sv_phi", -4.0));
  addFeatureSpec(FeatureSpec("tauh_sv_e", -1.0));
  addFeatureSpec(FeatureSpec("tauh_sv_ez", -1.0));
}

void Database::setupNetworkSpecs() {
  // node names
  std::vector<std::string> nodeNamesV0
      = { "hh_ggf", "hh_vbf", "tth_bb", "tth_tautau", "tt_dl", "tt_sl", "tt_fh", "dy" };
  std::vector<std::string> nodeNamesV0VBFBSM = { "hh_ggf", "hh_vbf", "hh_vbf_bsm", "tth_bb",
    "tth_tautau", "tt_dl", "tt_sl", "tt_fh", "dy" };
  std::vector<std::string> nodeNamesV5 = { "hh_ggf", "hh_vbf_sm", "hh_vbf_c2v", "tth_bb",
    "tth_tautau", "tt_dl", "tt_sl", "tt_fh", "dy" };

  // node merging
  std::vector<std::pair<std::string, std::vector<std::string>>> nodeMergingV0
      = { { "tth", { "tth_bb", "tth_tautau" } }, { "tt_lep", { "tt_dl", "tt_sl" } } };
  std::vector<std::pair<std::string, std::vector<std::string>>> nodeMergingV5
      = { { "hh_vbf", { "hh_vbf_sm", "hh_vbf_c2v" } }, { "tth", { "tth_bb", "tth_tautau" } },
          { "tt_lep", { "tt_dl", "tt_sl" } } };

  // feature names
  std::vector<std::string> featureNamesV0 = { "is_mutau", "is_etau", "is_tautau", "bjet1_pt",
    "bjet1_eta", "bjet1_phi", "bjet1_e", "bjet1_deepflavor_b", "bjet1_deepflavor_cvsb",
    "bjet1_deepflavor_cvsl", "bjet1_hhbtag", "bjet2_pt", "bjet2_eta", "bjet2_phi", "bjet2_e",
    "bjet2_deepflavor_b", "bjet2_deepflavor_cvsb", "bjet2_deepflavor_cvsl", "bjet2_hhbtag",
    "vbfjet1_pt", "vbfjet1_eta", "vbfjet1_phi", "vbfjet1_e", "vbfjet1_deepflavor_b",
    "vbfjet1_deepflavor_cvsb", "vbfjet1_deepflavor_cvsl", "vbfjet1_hhbtag", "vbfjet2_pt",
    "vbfjet2_eta", "vbfjet2_phi", "vbfjet2_e", "vbfjet2_deepflavor_b", "vbfjet2_deepflavor_cvsb",
    "vbfjet2_deepflavor_cvsl", "vbfjet2_hhbtag", "lep1_pt", "lep1_eta", "lep1_phi", "lep1_e",
    "lep2_pt", "lep2_eta", "lep2_phi", "lep2_e", "met_pt", "met_phi", "bh_pt", "bh_eta", "bh_phi",
    "bh_e", "tauh_sv_pt", "tauh_sv_eta", "tauh_sv_phi", "tauh_sv_ez" };
  std::vector<std::string> featureNamesV2 = { "is_mutau", "is_etau", "is_tautau", "is_2016",
    "is_2017", "is_2018", "bjet1_pt", "bjet1_eta", "bjet1_phi", "bjet1_e", "bjet1_deepflavor_b",
    "bjet1_hhbtag", "bjet2_pt", "bjet2_eta", "bjet2_phi", "bjet2_e", "bjet2_deepflavor_b",
    "bjet2_hhbtag", "ctjet1_pt", "ctjet1_eta", "ctjet1_phi", "ctjet1_e", "ctjet1_deepflavor_b",
    "ctjet1_hhbtag", "ctjet2_pt", "ctjet2_eta", "ctjet2_phi", "ctjet2_e", "ctjet2_deepflavor_b",
    "ctjet2_hhbtag", "ctjet3_pt", "ctjet3_eta", "ctjet3_phi", "ctjet3_e", "ctjet3_deepflavor_b",
    "ctjet3_hhbtag", "fwjet1_pt", "fwjet1_eta", "fwjet1_phi", "fwjet1_e", "fwjet2_pt", "fwjet2_eta",
    "fwjet2_phi", "fwjet2_e", "vbfjet1_pt", "vbfjet1_eta", "vbfjet1_phi", "vbfjet1_e",
    "vbfjet1_deepflavor_b", "vbfjet1_hhbtag", "vbfjet2_pt", "vbfjet2_eta", "vbfjet2_phi",
    "vbfjet2_e", "vbfjet2_deepflavor_b", "vbfjet2_hhbtag", "lep1_pt", "lep1_eta", "lep1_phi",
    "lep1_e", "lep2_pt", "lep2_eta", "lep2_phi", "lep2_e", "met_pt", "met_phi", "bh_pt", "bh_eta",
    "bh_phi", "bh_e", "tauh_sv_pt", "tauh_sv_eta", "tauh_sv_phi", "tauh_sv_ez" };
  std::vector<std::string> featureNamesV3B = { "is_mutau", "is_etau", "is_tautau", "is_2016",
    "is_2017", "is_2018", "bjet1_pt", "bjet1_eta", "bjet1_phi", "bjet1_e", "bjet1_deepflavor_b",
    "bjet1_hhbtag", "bjet2_pt", "bjet2_eta", "bjet2_phi", "bjet2_e", "bjet2_deepflavor_b",
    "bjet2_hhbtag", "ctjet1_pt", "ctjet1_eta", "ctjet1_phi", "ctjet1_e", "ctjet1_deepflavor_b",
    "ctjet1_hhbtag", "ctjet2_pt", "ctjet2_eta", "ctjet2_phi", "ctjet2_e", "ctjet2_deepflavor_b",
    "ctjet2_hhbtag", "ctjet3_pt", "ctjet3_eta", "ctjet3_phi", "ctjet3_e", "ctjet3_deepflavor_b",
    "ctjet3_hhbtag", "fwjet1_pt", "fwjet1_eta", "fwjet1_phi", "fwjet1_e", "fwjet2_pt", "fwjet2_eta",
    "fwjet2_phi", "fwjet2_e", "vbfjet1_pt", "vbfjet1_eta", "vbfjet1_phi", "vbfjet1_e",
    "vbfjet1_deepflavor_b", "vbfjet1_hhbtag", "vbfjet2_pt", "vbfjet2_eta", "vbfjet2_phi",
    "vbfjet2_e", "vbfjet2_deepflavor_b", "vbfjet2_hhbtag", "lep1_pt", "lep1_eta", "lep1_phi",
    "lep1_e", "lep2_pt", "lep2_eta", "lep2_phi", "lep2_e", "met_pt", "met_phi", "bh_pt", "bh_eta",
    "bh_phi", "bh_e", "tauh_sv_pt", "tauh_sv_eta", "tauh_sv_phi", "tauh_sv_e" };

  for (int year = 2016; year <= 2018; year++) {
    // 1) first training with minimal feature set and default output nodes
    addNetworkSpec(NetworkSpec(year, "v0", "kl1_c2v1_c31", nodeNamesV0, nodeMergingV0,
                       "LBN_input:0", "hmc/output/Softmax:0"),
        featureNamesV0);

    // 2) same as 1) but with additional hh_vbf_bsm output node
    addNetworkSpec(NetworkSpec(year, "v0", "kl1_c2v1_c31_vbfbsm", nodeNamesV0VBFBSM, nodeMergingV0,
                       "LBN_input:0", "hmc/output/Softmax:0"),
        featureNamesV0);

    // 3) same as 1) but trained in common_vbf data category
    // still, we call this v1
    addNetworkSpec(NetworkSpec(year, "v1", "kl1_c2v1_c31", nodeNamesV0, nodeMergingV0,
                       "LBN_input:0", "hmc/output/Softmax:0"),
        featureNamesV0);

    // 4) trained with vbf_common_os data category, more features to lbn_light tag, optimized
    // parameters
    addNetworkSpec(NetworkSpec(year, "v2", "kl1_c2v1_c31", nodeNamesV0, nodeMergingV0, "inputs:0",
                       "hmc/output/Softmax:0"),
        featureNamesV2);

    // 5) trained with vbf_loose_os data category, to be checked for pre-approval
    addNetworkSpec(NetworkSpec(year, "v3", "kl1_c2v1_c31_vbf", nodeNamesV0, nodeMergingV0,
                       "input:0", "model/output/truediv:0"),
        featureNamesV2);

    // 6) trained with vr_os data category, to be checked for pre-approval
    addNetworkSpec(NetworkSpec(year, "v3", "kl1_c2v1_c31_vr", nodeNamesV0, nodeMergingV0, "input:0",
                       "model/output/truediv:0"),
        featureNamesV2);

    // 7) same as 5), but training in combined vbf_os category with updated b-tag requirements
    // not yet fully optimized but using same hyperparameters as 5)
    addNetworkSpec(NetworkSpec(year, "v3b", "kl1_c2v1_c31_vbf", nodeNamesV0, nodeMergingV0,
                       "input:0", "model/output/truediv:0"),
        featureNamesV3B);

    // 8) same as 6), but training in combined vr_os category with updated b-tag requirements
    // not yet fully optimized but using same hyperparameters as 6)
    addNetworkSpec(NetworkSpec(year, "v3b", "kl1_c2v1_c31_vr", nodeNamesV0, nodeMergingV0,
                       "input:0", "model/output/truediv:0"),
        featureNamesV3B);

    // 9) same as 7), but training optimized, to be checked for pre-approval
    addNetworkSpec(NetworkSpec(year, "v4", "kl1_c2v1_c31_vbf", nodeNamesV0, nodeMergingV0,
                       "input:0", "model/output/truediv:0"),
        featureNamesV3B);

    // 10) same as 8), but training optimized, to be checked for pre-approval
    addNetworkSpec(NetworkSpec(year, "v4", "kl1_c2v1_c31_vr", nodeNamesV0, nodeMergingV0, "input:0",
                       "model/output/truediv:0"),
        featureNamesV3B);

    // 11) same as 9), but trained also with VBF C2V = {0,1} events and merged again
    addNetworkSpec(NetworkSpec(year, "v5", "kl1_c2v1_c31_vbf", nodeNamesV5, nodeMergingV5, "input:0",
                       "model/output/truediv:0"),
        featureNamesV3B);
  }
}

} // namespace hmc
