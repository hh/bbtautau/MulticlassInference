/*
 * Feature specification.
 */

#ifndef MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_FEATURESPEC_H
#define MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_FEATURESPEC_H

#include <algorithm>
#include <cmath>
#include <set>
#include <string>

namespace hmc {

class FeatureSpec {
  private:
  std::string name_;
  float defaultValue_;
  std::set<float> missingValues_;

  public:
  FeatureSpec(const std::string& name, float defaultValue, const std::set<float>& missing = {})
      : name_(name)
      , defaultValue_(defaultValue) {
    // add default missing values
    addMissingValue(-999.);
    addMissingValue(-1000.);
    addMissingValue(-9999.);
    addMissingValue(-10000.);

    // add additional missing values
    for (auto f : missing) {
      addMissingValue(f);
    }
  }

  ~FeatureSpec() {
  }

  inline const std::string& getName() const {
    return name_;
  }

  inline float getDefaultValue() const {
    return defaultValue_;
  }

  inline std::string getDescription() const {
    return "FeatureSpec(name: " + getName() + ", default: " + std::to_string(getDefaultValue())
        + ")";
  }

  inline const std::set<float>& getMissingValues() const {
    return missingValues_;
  }

  inline void addMissingValue(float f) {
    missingValues_.insert(f);
  }

  inline bool isMissingValue(float f) const {
    return missingValues_.find(f) != missingValues_.end() || std::isnan(f);
  }
};

} // namespace hmc

#endif // MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_FEATURESPEC_H
