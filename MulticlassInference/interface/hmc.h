/*
 * Provisioning includes.
 */

#ifndef MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_HMC_H
#define MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_HMC_H

#include "MulticlassInference/MulticlassInference/interface/Loader.h"
#include "MulticlassInference/MulticlassInference/interface/Features.h"

namespace hmc {

  inline Model* loadModel(int year, const std::string& version, const std::string& tag) {
    return Loader::instance().loadModel(year, version, tag);
  }

} // namespace hmc

#endif // MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_HMC_H
