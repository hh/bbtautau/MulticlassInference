/*
 * Input and output specification.
 */

#ifndef MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_IOSPEC_H
#define MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_IOSPEC_H

#include <map>
#include <set>
#include <vector>
#include <string>
#include <stdexcept> 

namespace hmc {

class InputSpec {
  private:
  const std::vector<std::string>& featureNames_;
  std::map<std::string, int> featureIndices_;
  std::vector<float> inputValues_;
  std::set<int> setIndices_;

  public:
  explicit InputSpec(const std::vector<std::string>& featureNames)
      : featureNames_(featureNames) {
    // build the index map
    for (int i = 0; i < (int)featureNames.size(); i++) {
      featureIndices_.insert(std::pair<std::string, int>(featureNames[i], i));
    }

    clear();
  }

  ~InputSpec() {
  }

  inline const std::vector<float> getValues() const {
    return inputValues_;
  }

  inline const std::vector<std::string>& getFeatureNames() const {
    return featureNames_;
  }

  inline int getNumberOfFeatures() const {
    return featureIndices_.size();
  }

  inline int getNumberOfSetFeatures() const {
    return setIndices_.size();
  }

  void clear() {
    inputValues_.clear();
    inputValues_.resize(getNumberOfFeatures());
    setIndices_.clear();
  }

  inline bool complete() const {
    return setIndices_.size() == featureIndices_.size();
  }

  void setValue(const std::string& featureName, float value) {
    const auto& it = featureIndices_.find(featureName);
    if (it == featureIndices_.end()) {
      throw std::runtime_error(
          "MulticlassInference InputSpec: no feature with name '" + featureName + "' found");
    }
    inputValues_[it->second] = value;
    setIndices_.insert(it->second);
  }

  float getValue(const std::string& featureName) const {
    const auto& it = featureIndices_.find(featureName);
    if (it == featureIndices_.end()) {
      throw std::runtime_error(
          "MulticlassInference InputSpec: no feature with name '" + featureName + "' found");
    }
    return inputValues_[it->second];
  }
};

class OutputSpec {
  private:
  std::vector<std::string> nodeNames_;
  std::map<std::string, float> output_;

  public:
  explicit OutputSpec(const std::vector<std::string>& nodeNames)
      : nodeNames_(nodeNames) {
    for (const auto& nodeName : nodeNames_) {
      output_.emplace(nodeName, 0.);
    }
  }

  ~OutputSpec() {
  }

  inline const std::map<std::string, float>::const_iterator begin() {
    return output_.begin();
  }

  inline const std::map<std::string, float>::const_iterator end() {
    return output_.end();
  }

  inline void clear() {
    for (const auto& nodeName : nodeNames_) {
      output_.at(nodeName) = 0.;
    }
  }

  inline void setOutput(const std::vector<float>& output) {
    if (output.size() != nodeNames_.size()) {
      throw std::runtime_error("MulticlassInference OutputSpec: number of outputs "
          + std::to_string(output.size()) + " does not match number of nodes "
          + std::to_string(nodeNames_.size()));
    }
    for (size_t i = 0; i < output.size(); i++) {
      output_.at(nodeNames_[i]) = output[i];
    }
  }

  inline float getOutput(const std::string& name) const {
    const auto it = output_.find(name);
    if (it == output_.end()) {
      throw std::runtime_error(
          "MulticlassInference OutputSpec: unknown output node name '" + name + "'");
    }
    return it->second;
  }

  inline float* getOutputAddress(const std::string& name) {
    auto it = output_.find(name);
    if (it == output_.end()) {
      throw std::runtime_error(
          "MulticlassInference OutputSpec: unknown output node name '" + name + "'");
    }
    return &it->second;
  }
};

} // namespace hmc

#endif // MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_IOSPEC_H
