/*
 * TensorFlow helpers, extracted from PhysicsTools/TensorFlow.
 */

#ifndef MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_TENSORFLOW_H
#define MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_TENSORFLOW_H

#include "tensorflow/cc/client/client_session.h"
#include "tensorflow/cc/saved_model/constants.h"
#include "tensorflow/cc/saved_model/loader.h"
#include "tensorflow/cc/saved_model/tag_constants.h"
#include "tensorflow/core/framework/tensor.h"
#include "tensorflow/core/lib/core/threadpool.h"
#include "tensorflow/core/lib/core/threadpool_options.h"
#include "tensorflow/core/lib/io/path.h"
#include "tensorflow/core/public/session.h"
#include "tensorflow/core/util/tensor_bundle/naming.h"

namespace hmctf {

typedef std::pair<std::string, tensorflow::Tensor> NamedTensor;
typedef std::vector<NamedTensor> NamedTensorList;

class NoThreadPool : public tensorflow::thread::ThreadPoolInterface {
  public:
  static NoThreadPool& instance() {
    static NoThreadPool pool;
    return pool;
  }

  explicit NoThreadPool()
      : numScheduleCalled_(0) {
  }

  void Schedule(std::function<void()> fn) override {
    numScheduleCalled_ += 1;
    fn();
  }

  void ScheduleWithHint(std::function<void()> fn, int start, int end) override {
    Schedule(fn);
  }

  void Cancel() override {
  }

  int NumThreads() const override {
    return 1;
  }

  int CurrentThreadId() const override {
    return -1;
  }

  int GetNumScheduleCalled() {
    return numScheduleCalled_;
  }

  private:
  std::atomic<int> numScheduleCalled_;
};

// set the tensorflow log level
void setLogging(const std::string& level = "3");

// loads a graph definition saved as a protobuf file at pbFile
// transfers ownership
tensorflow::GraphDef* loadGraphDef(const std::string& pbFile);

// return a new session that will contain an already loaded graph def, threading options are
// inferred from nThreads
// an error is thrown when graphDef is a nullptr or when the grah has no nodes
// transfers ownership
tensorflow::Session* createSession(tensorflow::GraphDef* graphDef, int nThreads = 1);

// closes a session, calls its destructor, resets the pointer, and returns true on success
bool closeSession(tensorflow::Session*& session);

// run the session with inputs and outputNames and store output tensors
// internally, the NoThreadPool is used
void run(tensorflow::Session* session, const NamedTensorList& inputs,
    const std::vector<std::string>& outputNames, std::vector<tensorflow::Tensor>* outputs);

} // namespace hmctf

#endif // MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_TENSORFLOW_H
