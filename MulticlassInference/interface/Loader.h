/*
 * Model loader and database for NetworkSpec's and FeatureSpec's.
 */

#ifndef MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_LOADER_H
#define MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_LOADER_H

#include <vector>

#include "MulticlassInference/MulticlassInference/interface/Database.h"
#include "MulticlassInference/MulticlassInference/interface/Model.h"

namespace hmc {

class Loader {
  private:
  const Database& database_;
  std::vector<Model*> models_;

  public:
  static Loader& instance() {
    static Loader loader;
    return loader;
  }

  explicit Loader()
      : database_(Database::instance()) {
  }

  Loader(const Loader&) = delete;

  ~Loader() {
    for (Model* model : models_) {
      delete model;
    }
    models_.clear();
  }

  bool hasModel(int year, const std::string& version, const std::string& tag) const;

  Model* loadModel(int year, const std::string& version, const std::string& tag);

  Model* getModel(int year, const std::string& version, const std::string& tag);
};

} // namespace hmc

#endif // MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_LOADER_H
