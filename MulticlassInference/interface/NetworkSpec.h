/*
 * Network specification.
 */

#ifndef MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_NETWORKSPEC_H
#define MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_NETWORKSPEC_H

#include <fstream>
#include <vector>

#include "MulticlassInference/MulticlassInference/interface/FeatureSpec.h"

namespace hmc {

class NetworkSpec {
  private:
  int year_;
  std::string version_;
  std::string tag_;
  std::vector<std::string> nodeNames_;
  std::vector<std::pair<std::string, std::vector<int>>> nodeMerging_;
  std::string inputTensorName_;
  std::string outputTensorName_;

  std::string graphPathEven_;
  std::string graphPathOdd_;

  std::vector<FeatureSpec> featureSpecs_;

  public:
  NetworkSpec(int year, const std::string& version, const std::string& tag,
      const std::vector<std::string>& nodeNames,
      const std::vector<std::pair<std::string, std::vector<std::string>>>& nodeMerging = {},
      const std::string& inputTensorName = "input", const std::string& outputTensorName = "output")
      : year_(year)
      , version_(version)
      , tag_(tag)
      , nodeNames_(nodeNames)
      , inputTensorName_(inputTensorName)
      , outputTensorName_(outputTensorName)
      , graphPathEven_(getGraphPath(true))
      , graphPathOdd_(getGraphPath(false)) {
    // set the nodeMerging which stores indices internally instead of names
    for (const auto& pair : nodeMerging) {
      std::vector<int> indices;
      for (const auto& nodeName : pair.second) {
        const auto& it = std::find(nodeNames_.begin(), nodeNames_.end(), nodeName);
        if (it == nodeNames_.end()) {
          throw std::runtime_error("MulticlassInference NetworkSpec: unknown node name '" + nodeName
              + "' configured for merging into '" + pair.first + "'");
        }
        indices.push_back(std::distance(nodeNames_.begin(), it));
      }
      nodeMerging_.emplace_back(pair.first, indices);
    }
  }

  ~NetworkSpec() {
  }

  inline int getYear() const {
    return year_;
  }

  inline const std::string& getVersion() const {
    return version_;
  }

  inline const std::string& getTag() const {
    return tag_;
  }

  inline const std::vector<std::string>& getNodeNames() const {
    return nodeNames_;
  }

  inline const std::vector<std::pair<std::string, std::vector<int>>>&
  getNodeMerging() const {
    return nodeMerging_;
  }

  std::vector<std::string> getAllNodeNames() const {
    std::vector<std::string> allNames = getNodeNames();
    for (const auto& pair : getNodeMerging()) {
      allNames.push_back(pair.first);
    }
    return allNames;
  }

  inline const std::string& getInputTensorName() const {
    return inputTensorName_;
  }

  inline const std::string& getOutputTensorName() const {
    return outputTensorName_;
  }

  inline size_t getNumberOfNodes() const {
    return nodeNames_.size();
  }

  inline std::string getDescription() const {
    return "NetworkSpec(year: " + std::to_string(getYear()) + ", version: " + getVersion()
        + ", tag: " + getTag() + ", features: " + std::to_string(getNumberOfFeatureSpecs()) + ")";
  }

  inline const std::vector<FeatureSpec>& getFeatureSpecs() const {
    return featureSpecs_;
  }

  inline size_t getNumberOfFeatureSpecs() const {
    return featureSpecs_.size();
  }

  std::vector<std::string> getFeatureNames() const {
    std::vector<std::string> featureNames;
    for (const auto& featureSpec : featureSpecs_) {
      featureNames.push_back(featureSpec.getName());
    }
    return featureNames;
  }

  const std::string& getFeatureName(int i) const {
    return featureSpecs_.at(i).getName();
  }

  inline bool hasFeatureSpec(const std::string& name) const {
    for (const auto& featureSpec : featureSpecs_) {
      if (featureSpec.getName() == name) {
        return true;
      }
    }
    return false;
  }

  void addFeatureSpec(const FeatureSpec& featureSpec) {
    const std::string name = featureSpec.getName();
    if (hasFeatureSpec(name)) {
      throw std::runtime_error("MulticlassInference " + getDescription() + ": feature with name '"
          + name + "' already registered");
    }

    featureSpecs_.push_back(featureSpec);
  }

  inline const FeatureSpec& getFeatureSpec(const std::string& name) const {
    for (const auto& featureSpec : featureSpecs_) {
      if (featureSpec.getName() == name) {
        return featureSpec;
      }
    }
    throw std::runtime_error(
        "MulticlassInference " + getDescription() + ": no feature with name '" + name + "' found");
  }

  inline const std::string& getGraphPathEven() const {
    return graphPathEven_;
  }
  inline const std::string& getGraphPathOdd() const {
    return graphPathOdd_;
  }

  std::string getGraphPath(bool even, bool check = true) const {
    std::string graphPath = std::string(std::getenv("CMSSW_BASE"))
        + "/src/MulticlassInference/MulticlassInference/data" + "/graph__"
        + std::to_string(getYear()) + "__" + getVersion() + "__" + getTag() + "__"
        + (even ? "even" : "odd") + ".pb";

    if (check) {
      std::ifstream f(graphPath.c_str());
      if (!f.good()) {
        throw std::runtime_error("MulticlassInference " + getDescription() + ": graphPath '"
            + graphPath + "' does not exist");
      }
    }

    return graphPath;
  }
};

} // namespace hmc

#endif // MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_NETWORKSPEC_H
