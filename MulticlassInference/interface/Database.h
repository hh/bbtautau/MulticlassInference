/*
 * Database for NetworkSpec's and FeatureSpec's.
 */

#ifndef MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_DATABASE_H
#define MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_DATABASE_H

#include <map>
#include <vector>

#include "MulticlassInference/MulticlassInference/interface/FeatureSpec.h"
#include "MulticlassInference/MulticlassInference/interface/NetworkSpec.h"

namespace hmc {

class Database {
  private:
  bool setup_;
  std::map<std::string, FeatureSpec> featureSpecs_;
  std::vector<NetworkSpec> networkSpecs_;

  void setupFeatureSpecs();
  void setupNetworkSpecs();

  public:
  static Database& instance() {
    static Database database;
    database.setup();
    return database;
  }

  explicit Database()
      : setup_(false) {
  }

  Database(const Database&) = delete;

  ~Database() {
  }

  void setup() {
    if (setup_) {
      return;
    }
    setup_ = true;

    setupFeatureSpecs();
    setupNetworkSpecs();
  }

  inline size_t getNumberOfFeatureSpecs() const {
    return featureSpecs_.size();
  }

  void addFeatureSpec(const FeatureSpec& featureSpec) {
    const std::string name = featureSpec.getName();
    if (featureSpecs_.find(name) != featureSpecs_.end()) {
      throw std::runtime_error(
          "MulticlassInference Databse: FeatureSpec with name '" + name + "' already registered");
    }
    featureSpecs_.insert(std::pair<std::string, FeatureSpec>(name, featureSpec));
  }

  const FeatureSpec& getFeatureSpec(const std::string& name) const {
    auto it = featureSpecs_.find(name);
    if (it == featureSpecs_.end()) {
      throw std::runtime_error(
          "MulticlassInference Database: no FeatureSpec with name '" + name + "' found");
    }
    return it->second;
  }

  void addNetworkSpec(
      const NetworkSpec& networkSpec, const std::vector<std::string>& featureNames) {
    // already registered?
    for (const auto& n : networkSpecs_) {
      if (n.getYear() == networkSpec.getYear() && n.getVersion() == networkSpec.getVersion()
          && n.getTag() == networkSpec.getTag()) {
        throw std::runtime_error("MulticlassInference Database: NetworkSpec '" + n.getDescription()
            + "' already registered");
      }
    }

    // add it
    networkSpecs_.push_back(networkSpec);

    // add features by name
    for (const auto& featureName : featureNames) {
      networkSpecs_.back().addFeatureSpec(getFeatureSpec(featureName));
    }
  }

  const NetworkSpec& getNetworkSpec(
      int year, const std::string& version, const std::string& tag) const {
    for (const auto& n : networkSpecs_) {
      if (n.getYear() == year && n.getVersion() == version && n.getTag() == tag) {
        return n;
      }
    }
    throw std::runtime_error("MulticlassInference Database : no NetworkSpec found");
  }
};

} // namespace hmc

#endif // MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_DATABASE_H
