/*
 * Model definition.
 */

#ifndef MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_MODEL_H
#define MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_MODEL_H

#include "MulticlassInference/MulticlassInference/interface/IOSpec.h"
#include "MulticlassInference/MulticlassInference/interface/NetworkSpec.h"
#include "MulticlassInference/MulticlassInference/interface/TensorFlow.h"

namespace hmc {

typedef unsigned long long EventId;

class Model {
  private:
  const NetworkSpec* networkSpec_;
  tensorflow::GraphDef* graphDefEven_;
  tensorflow::GraphDef* graphDefOdd_;
  tensorflow::Session* sessionEven_;
  tensorflow::Session* sessionOdd_;

  public:
  InputSpec input;
  OutputSpec output;

  explicit Model(const NetworkSpec* networkSpec);

  ~Model();

  void printInfo() const;

  inline const NetworkSpec* getNetworkSpec() const {
    return networkSpec_;
  }

  inline int getYear() const {
    return getNetworkSpec()->getYear();
  }

  inline const std::string& getVersion() const {
    return getNetworkSpec()->getVersion();
  }

  inline const std::string& getTag() const {
    return getNetworkSpec()->getTag();
  }

  inline const size_t getNumberOfNodes() const {
    return getNetworkSpec()->getNumberOfNodes();
  }

  inline const std::vector<std::string>& getNodeNames() const {
    return getNetworkSpec()->getNodeNames();
  }

  inline const std::vector<std::pair<std::string, std::vector<int>>>& getNodeMerging() const {
    return getNetworkSpec()->getNodeMerging();
  }

  inline std::vector<std::string> getAllNodeNames() const {
    return getNetworkSpec()->getAllNodeNames();
  }

  inline const std::string& getNodeName(int i) const {
    return getNodeNames().at(i);
  }

  inline const std::string& getInputTensorName() const {
    return getNetworkSpec()->getInputTensorName();
  }

  inline const std::string& getOutputTensorName() const {
    return getNetworkSpec()->getOutputTensorName();
  }

  inline size_t getNumberOfFeatures() const {
    return getNetworkSpec()->getNumberOfFeatureSpecs();
  }

  inline const std::vector<FeatureSpec>& getFeatureSpecs() const {
    return getNetworkSpec()->getFeatureSpecs();
  };

  inline bool hasFeatureSpec(const std::string& name) const {
    return getNetworkSpec()->hasFeatureSpec(name);
  }

  inline std::vector<std::string> getFeatureNames() const {
    return getNetworkSpec()->getFeatureNames();
  }

  inline const std::string& getFeatureName(int i) const {
    return getNetworkSpec()->getFeatureName(i);
  }

  inline std::string getDescription() const {
    return "Model(year: " + std::to_string(getYear()) + ", version: " + getVersion()
        + ", tag: " + getTag() + ", features: " + std::to_string(getNumberOfFeatures()) + ")";
  }

  inline tensorflow::Tensor createInputTensor(size_t batchSize = 1) {
    tensorflow::Tensor t(tensorflow::DT_FLOAT,
        tensorflow::TensorShape({ (int)batchSize, (int)getNumberOfFeatures() }));
    return t;
  }

  inline void run(EventId eventId, bool checkDefaults = true) {
    return run(eventId, input, output, checkDefaults);
  }

  void run(EventId eventId, const InputSpec& inputSpec, OutputSpec& outputSpec,
      bool checkDefaults = true);

  void run(EventId eventId, const std::vector<float>& input, std::vector<float>& output,
      bool checkDefaults = true);

  tensorflow::Tensor run(EventId eventId, tensorflow::Tensor& input, bool checkDefaults = true);
};

} // namespace hmc

#endif // MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_MODEL_H
