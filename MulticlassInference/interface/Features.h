/*
 * Definition of complex features.
 */

#ifndef MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_FEATURES_H
#define MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_FEATURES_H

#include <cmath>

namespace hmc {

namespace features {

static const float EMPTY = -999.;

} // namespace features

} // namespace hmc

#endif // MULTICLASSINFERENCE_MULTICLASSINFERENCE_INTERFACE_FEATURES_H
