/*
 * Test of the MulticlassInference tool.
 */

#include <cppunit/extensions/HelperMacros.h>
#include <stdexcept>
#include <cmath>

#include <Utilities/Testing/interface/CppUnit_testdriver.icpp>

#include "MulticlassInference/MulticlassInference/interface/hmc.h"

class testMulticlassInference : public CppUnit::TestFixture {
  CPPUNIT_TEST_SUITE(testMulticlassInference);
  CPPUNIT_TEST(checkAll);
  CPPUNIT_TEST_SUITE_END();

  public:
  void checkAll();
  void testModel(int year, const std::string& version, const std::string& tag);

  inline bool endsWith(const std::string& s, const std::string& suffix) {
    return s.rfind(suffix) == std::labs(s.size() - suffix.size());
  }
};

CPPUNIT_TEST_SUITE_REGISTRATION(testMulticlassInference);

void testMulticlassInference::checkAll() {
  // currently, the models are identical per year, so just test 2018
  testModel(2018, "v0", "kl1_c2v1_c31");
  testModel(2018, "v0", "kl1_c2v1_c31_vbfbsm");
  testModel(2018, "v1", "kl1_c2v1_c31");
  testModel(2018, "v2", "kl1_c2v1_c31");
  testModel(2018, "v3", "kl1_c2v1_c31_vbf");
  testModel(2018, "v3", "kl1_c2v1_c31_vr");
  testModel(2018, "v3b", "kl1_c2v1_c31_vbf");
  testModel(2018, "v3b", "kl1_c2v1_c31_vr");
  testModel(2018, "v4", "kl1_c2v1_c31_vbf");
  testModel(2018, "v4", "kl1_c2v1_c31_vr");
  testModel(2018, "v5", "kl1_c2v1_c31_vbf");
}

void testMulticlassInference::testModel(int year, const std::string& version, const std::string& tag) {
  std::cout << std::endl << std::endl;
  std::cout << "========================================" << std::endl;

  // load the model
  hmc::Model* model = hmc::loadModel(year, version, tag);

  // define an event id, to be set to the actual id of the event
  int eventId = 12;

  // define some meaningful test features
  std::vector<float> testFeatures;
  for (const std::string& featureName : model->getFeatureNames()) {
    float f = 0.5;
    // meaningful four-vectors
    if (endsWith(featureName, "_pt")) {
      f = 30.;
    } else if (endsWith(featureName, "_eta")) {
      f = 0.;
    } else if (endsWith(featureName, "_phi")) {
      f = 0.;
    } else if (endsWith(featureName, "_e")) {
      f = 32.;
    }
    testFeatures.push_back(f);
  }

  //
  // single event evaluation using input and output specs that can be queried by name
  // (recommended for single event processing)
  //

  std::cout << std::endl << "single event evaluation with input and output specs" << std::endl;

  // fill features
  model->input.clear();
  for (size_t i = 0; i < model->getNumberOfFeatures(); i++) {
    std::string featureName = model->getFeatureName(int(i));
    model->input.setValue(featureName, testFeatures[i]);
  }

  // run the model
  model->run(eventId);

  // print the result
  for (const auto& it : model->output) {
    std::cout << "Node " << it.first << ", value " << it.second << std::endl;
  }

  //
  // single event evaluation using a float vector
  //

  std::cout << std::endl << "single event evaluation with float vectors" << std::endl;

  // define an output vector and run
  std::vector<float> outputValues;
  model->run(++eventId, testFeatures, outputValues);

  // print the result
  for (size_t i = 0; i < outputValues.size(); i++) {
    std::cout << "Node " << model->getNodeName(i) << ", value " << outputValues[i] << std::endl;
  }

  //
  // batched evaluation with a tensor
  //

  std::cout << std::endl << "batched event evaluation with tensors" << std::endl;

  // create a tensor with a batch size of 2 and fill some meaningless floats
  tensorflow::Tensor inputTensor = model->createInputTensor(2);
  float* d = inputTensor.flat<float>().data();
  for (int b = 0; b < 2; b++) {
    for (size_t i = 0; i < model->getNumberOfFeatures(); i++, d++) {
      *d = testFeatures[i];
    }
  }

  // run
  tensorflow::Tensor outputTensor = model->run(++eventId, inputTensor);

  // print the result
  d = outputTensor.flat<float>().data();
  for (int b = 0; b < 2; b++) {
    std::cout << "batch " << b << std::endl;
    for (size_t i = 0; i < outputValues.size(); i++, d++) {
      std::cout << "Node " << model->getNodeName(i) << ", value " << *d << std::endl;
    }
  }
}
