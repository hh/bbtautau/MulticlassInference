# Inference tools for the HH → bb𝝉𝝉 multiclass classifier

### Resources

- [HH → bb𝝉𝝉 multiclass classifier](https://gitlab.cern.ch/hh/bbtautau/multiclass)
- [CMSSW TensorFlow interface](https://gitlab.cern.ch/mrieger/CMSSW-DNN)


### Setup

```shell
export SCRAM_ARCH="slc7_amd64_gcc820"
cmsrel CMSSW_11_1_0_pre6
cd CMSSW_11_1_0_pre6/src
cmsenv

git clone https://gitlab.cern.ch/hh/bbtautau/MulticlassInference.git

scram b -j

# optionally run the test
./../test/$SCRAM_ARCH/testMulticlassInference
```


### Usage

The models of the multiclass classifier are separated per year and version (currentl latest version is `V0`). To load model, simply run:

```cpp
#include "MulticlassInference/MulticlassInference/interface/hmc.h"

// ...

// load the model v0 for 2018, tagged "kl1_c2v1_c31"
hmc::Model* model = hmc::loadModel(2018, "v0", "kl1_c2v1_c31");
```

To run the model with certain inputs and inspect the classification outputs (i.e., process probabilities), do:

```cpp
model->input.clear();
model->input.setValue("is_mutau", 1.);
model->input.setValue("is_etau", 0.);
model->input.setValue("is_tautau", 0.);
model->input.setValue("bjet1_pt", 143.89);
// ...

// run the model
int eventId = 12;  // to be set to the actual id of the event
model->run(eventId);

// print the result
for (const auto& it : model->output) {
    std::cout << "Node " << it.first << ", value " << it.second << std::endl;
}
// prints
// Node hh_ggf,     value 0.142411
// Node hh_vbf,     value 0.0584048
// Node hh_vbf_bsm, value 0.0891544
// Node tth_bb,     value 0.236118
// Node tth_tautau, value 0.098309
// Node tt_dl,      value 0.11335
// Node tt_sl,      value 0.112228
// Node tt_fh,      value 0.0874079
// Node dy,         value 0.0626178
```

**Important notes**

- The features per year, version and tag are defined in [`Database.cc`](MulticlassInference/src/Database.cc).
- The inference tool handles proper default values on its own.
- If you want to run the evaluation batched, or in a more manual fashion based on float vectors, see the [`MulticlassInference.cc`](MulticlassInference/test/testMulticlassInference.cc) test script.
